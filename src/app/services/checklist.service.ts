import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class ChecklistService {

  trelloKey: string = "042ad0a4e07ee114cc5df4534c75a0bc";
  trelloToken: string = "012605bd179953705efb552189fea2673036a572433f73a94fcc927ffe23ec3f";
  cardUrl: string = 'https://api.trello.com/1/cards';
  checklistUrl: string = 'https://api.trello.com/1/checklists';
  
  // https://api.trello.com/1/cards/{id}/checkItem/{idCheckItem}
  constructor(private http: HttpClient) { }

  getChecklists(id: string): Observable<any> {
    return this.http.get(`${this.cardUrl}/${id}/checklists?key=${this.trelloKey}&token=${this.trelloToken}`);
  }

  deleteChecklist(id: string): Observable<any> {
    return this.http.delete(`${this.checklistUrl}/${id}?key=${this.trelloKey}&token=${this.trelloToken}`);
  }

  addChecklist(id: string, name: string): Observable<any> {
    return this.http.post(`${this.checklistUrl}?idCard=${id}&name=${name}&key=${this.trelloKey}&token=${this.trelloToken}`, null);
  }

  updateChecklist(id: string, name: string): Observable<any> {
    return this.http.put(`${this.checklistUrl}/${id}?name=${name}&key=${this.trelloKey}&token=${this.trelloToken}`, null);
  }

  addCheckItem(id: string, name: string): Observable<any> {
    return this.http.post(`${this.checklistUrl}/${id}/checkItems?key=${this.trelloKey}&token=${this.trelloToken}&name=${name}`, null);
  }

  updateCheckItemName(id: string, itemId: string,name: string): Observable<any> {
    return this.http.put(`${this.cardUrl}/${id}/checkItem/${itemId}?key=${this.trelloKey}&token=${this.trelloToken}&name=${name}`, null);
  }

  deleteCheckItem(id: string, itemId: string): Observable<any> {
    return this.http.delete(`${this.cardUrl}/${id}/checkItem/${itemId}?key=${this.trelloKey}&token=${this.trelloToken}`);
  }
}
