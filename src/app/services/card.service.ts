import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class CardService {

  trelloKey: string = "042ad0a4e07ee114cc5df4534c75a0bc";
  trelloToken: string = "012605bd179953705efb552189fea2673036a572433f73a94fcc927ffe23ec3f";
  cardsFromListsUrl : string = 'https://api.trello.com/1/lists';
  cardUrl: string = 'https://api.trello.com/1/cards';

  constructor(private http: HttpClient) { }

  getCards(id: string): Observable<any> {
    return this.http.get(`${this.cardsFromListsUrl}/${id}/cards?key=${this.trelloKey}&token=${this.trelloToken}`);
  }

  addCard(name: string, id: string): Observable<any> {
    return this.http.post(`${this.cardUrl}?idList=${id}&name=${name}&key=${this.trelloKey}&token=${this.trelloToken}`, null);
  }

  updateNameOfCard(id: string, name: string): Observable<any> {
    return this.http.put(`${this.cardUrl}/${id}?name=${name}&key=${this.trelloKey}&token=${this.trelloToken}`, null);
  }

  deleteCard(id: string): Observable<any> {
    return this.http.delete(`${this.cardUrl}/${id}?key=${this.trelloKey}&token=${this.trelloToken}`);
  }
}
