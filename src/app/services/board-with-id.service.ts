import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BoardWithIDService {

  trelloKey: string = "042ad0a4e07ee114cc5df4534c75a0bc";
  trelloToken: string = "012605bd179953705efb552189fea2673036a572433f73a94fcc927ffe23ec3f";
  boardUrl: string = "https://api.trello.com/1/boards/";

  constructor(private http: HttpClient) { }

  getBoardInfo(id): Observable<any> {
    return this.http.get(`${this.boardUrl}${id}?key=${this.trelloKey}&token=${this.trelloToken}`);
  }

  editBoardName(id, name): Observable<any> {
    return this.http.put(`${this.boardUrl}${id}?name=${name}&key=${this.trelloKey}&token=${this.trelloToken}`, null);
  }

  deleteBoard(id): Observable<any> {
    return this.http.delete(`${this.boardUrl}${id}?key=${this.trelloKey}&token=${this.trelloToken}`);
  }
}
