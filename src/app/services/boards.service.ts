import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BoardsService {

  trelloKey: string = "042ad0a4e07ee114cc5df4534c75a0bc";
  trelloToken: string = "012605bd179953705efb552189fea2673036a572433f73a94fcc927ffe23ec3f";
  getBoardsUrl: string = "https://api.trello.com/1/members/me/boards";
  addBoardUrl: string = "https://api.trello.com/1/boards";

  constructor(private http: HttpClient) { }

  getBoards(): Observable<any> {
    return this.http.get(`${this.getBoardsUrl}?key=${this.trelloKey}&token=${this.trelloToken}`);
  }

  addBoard(name): Observable<any> {
    return this.http.post(`${this.addBoardUrl}/?name=${name}&key=${this.trelloKey}&token=${this.trelloToken}`, null);
  }
}
