import { TestBed } from '@angular/core/testing';

import { BoardWithIDService } from './board-with-id.service';

describe('BoardWithIDService', () => {
  let service: BoardWithIDService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BoardWithIDService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
