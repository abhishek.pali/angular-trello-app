import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ListService {

  trelloKey: string = "042ad0a4e07ee114cc5df4534c75a0bc";
  trelloToken: string = "012605bd179953705efb552189fea2673036a572433f73a94fcc927ffe23ec3f";
  boardUrl: string = 'https://api.trello.com/1/boards';
  listUrl: string = 'https://api.trello.com/1/lists';

  constructor(private http: HttpClient) { }

  getLists(id: string): Observable<any> {
    return this.http.get(`${this.boardUrl}/${id}?lists=open&key=${this.trelloKey}&token=${this.trelloToken}`);
  }

  addList(name: string, id: string): Observable<any> {
    return this.http.post(`${this.listUrl}?name=${name}&idBoard=${id}&pos=bottom&key=${this.trelloKey}&token=${this.trelloToken}`, null);
  }

  deleteList(id: string): Observable<any> {
    return this.http.put(`${this.listUrl}/${id}/closed?value=true&key=${this.trelloKey}&token=${this.trelloToken}`, null);
  }

  updateList(id: string, name: string): Observable<any> {
    return this.http.put(`${this.listUrl}/${id}?name=${name}&key=${this.trelloKey}&token=${this.trelloToken}`, null)
  }
}
