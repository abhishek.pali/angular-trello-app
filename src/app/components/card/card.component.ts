import { Component, OnInit, Input } from '@angular/core';
import { CardService } from 'src/app/services/card.service';
import { MatDialog } from '@angular/material/dialog';
import { CardDialogComponent } from '../card-dialog/card-dialog.component';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {

  @Input() listId: string;

  cardsData: any[] = [];
  showAddCardInputDiv: boolean = false;
  showAddCardButton: boolean = true;
  cardName: string = "";

  constructor(private cardService: CardService, private dialog: MatDialog) { }

  ngOnInit(): void {
    this.cardService.getCards(this.listId).subscribe(data => {
      this.cardsData = data;
      console.log(data);
    });
  }

  handleInputChange(event) {
    this.cardName = event.target.value
  }

  toggleAddCardInputDiv() {
    this.showAddCardButton = !this.showAddCardButton;
    this.showAddCardInputDiv = !this.showAddCardInputDiv;
  }

  addNewCard() {
    this.showAddCardButton = !this.showAddCardButton;
    this.showAddCardInputDiv = !this.showAddCardInputDiv;
    this.cardService.addCard(this.cardName, this.listId).subscribe(data => {
      this.cardsData.push(data);
    })
  }

  openDialog(event) {
    console.log(event.target);
    const dialogRef = this.dialog.open(CardDialogComponent, {
      width: '800px',
      height: '800px',
      data: {name: event.target.innerHTML, id: event.target.id}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      if (result === "delete")
        {
          this.cardService.deleteCard(event.target.id).subscribe(() => {
            this.cardsData = this.cardsData.filter(card => card.id !== event.target.id);
          });

        }
      else if (result !== "" && result !== undefined)
        {
          this.cardService.updateNameOfCard(event.target.id, result).subscribe(data => {
            console.log('data : ', data);
            this.cardsData = this.cardsData.map(card => {
              if(card.id === event.target.id)
                {
                  card.name = result;
                }
              return card;
            });
          });
        }
      else 
        {
          // console.log("hi");
        }
    });
  }
}
