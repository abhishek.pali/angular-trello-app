import { Component, OnInit, Input } from '@angular/core';
import { ListService } from 'src/app/services/list.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  @Input() boardId: string;

  listsData: any[] = [];
  showInputDiv: boolean = false;
  showListHeading: boolean = true;
  listName: string = "";
  showAddListInputDiv: boolean = false;
  showAddListButton: boolean = true;
  targetId: string = "";

  constructor(private listService: ListService) { }

  ngOnInit(): void {
    this.listService.getLists(this.boardId).subscribe(data => {
      this.listsData = data.lists;
      console.log(data);
    });
  }

  toggleInputDiv(event) {
    this.showInputDiv = !this.showInputDiv;
    this.showListHeading = !this.showListHeading;
    this.targetId = event.target.id;
    console.log(this.targetId);
  }

  handleInputChange(event) {
    this.listName = event.target.value
  }

  toggleAddListInputDiv() {
    this.showAddListButton = !this.showAddListButton;
    this.showAddListInputDiv = !this.showAddListInputDiv;
  }

  addNewList() {
    this.showAddListButton = !this.showAddListButton;
    this.showAddListInputDiv = !this.showAddListInputDiv;
    this.listService.addList(this.listName, this.boardId).subscribe(data => {
      this.listsData.push(data);
    });
  }

  deleteThisList(event) {
    this.listService.deleteList(event.target.id).subscribe(() => {
      this.listsData = this.listsData.filter(list => list.id !== event.target.id);
    });
    
  }

  updateListName() {
    this.showInputDiv = !this.showInputDiv;
    this.showListHeading = !this.showListHeading;
    this.listService.updateList(this.targetId, this.listName).subscribe(() => {
      this.listsData = this.listsData.map(list => {
        if (list.id === this.targetId)
          {
            list.name = this.listName;
          }
        return list;
      });
    });
  }

}
