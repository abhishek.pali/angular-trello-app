import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { BoardWithIDService } from 'src/app/services/board-with-id.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css'],
  providers: [Location]
})
export class BoardComponent implements OnInit {

  boardId: string = "";
  boardData: any = {};
  showHeading: boolean = true;
  showInputDiv: boolean = false;
  boardName: string = "";
  showMainBoard: boolean = true;
  showDeletedText: boolean = false;

  constructor(private location: Location, private boardWithIdService: BoardWithIDService, private router: Router) {
      console.log(this.location.path());
     this.boardId = this.location.path().slice(-24);
  }

  ngOnInit(): void {
    this.boardWithIdService.getBoardInfo(this.boardId).subscribe(data => {
      this.boardData = data;
      console.log(data);
    });
  }

  toggleInput(event) {
    this.showHeading = !this.showHeading;
    this.showInputDiv = !this.showInputDiv;
    console.log(event.target.innerHTML);
    this.boardName = event.target.innerHTML;
  }

  editName(event) {
    this.boardName = event.target.value;
    console.log(this.boardName);
  }

  saveName() {
    this.showHeading = !this.showHeading;
    this.showInputDiv = !this.showInputDiv;
    this.boardWithIdService.editBoardName(this.boardId, this.boardName).subscribe(data => {
      this.boardData = data;
      console.log(data);
    });
  }

  deleteThisBoard() {
    this.boardWithIdService.deleteBoard(this.boardId).subscribe();
    this.showMainBoard = !this.showMainBoard;
    this.showDeletedText = !this.showDeletedText;
  }


}
