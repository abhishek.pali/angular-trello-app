import { Component, OnInit } from '@angular/core';
import { BoardsService } from '../../services/boards.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {

  boardsData: any[] = [];
  showButton: boolean = true;
  showInputDiv: boolean = false;
  newBoardName: string = "";

  constructor(private boardsService: BoardsService) { }

  ngOnInit(): void {
    this.boardsService.getBoards().subscribe(data => {
      this.boardsData = data;
      console.log(data);
    });
  }

  toggleInputDiv() {
    this.showButton = !this.showButton;
    this.showInputDiv = !this.showInputDiv;
  }

  handleInputChange(event) {
    this.newBoardName = event.target.value;
  }

  addNewBoard() {
    this.showButton = !this.showButton;
    this.showInputDiv = !this.showInputDiv;
    this.boardsService.addBoard(this.newBoardName).subscribe(data => {
      this.boardsData.push(data);
      console.log(data);
    });
  }
}
