import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ChecklistService } from 'src/app/services/checklist.service';

@Component({
  selector: 'app-card-dialog',
  templateUrl: './card-dialog.component.html',
  styleUrls: ['./card-dialog.component.css']
})
export class CardDialogComponent implements OnInit {

  cardName: string = "";
  showCardDialogHeading: boolean = true;
  showCardDialogNameInputDiv: boolean = false;
  showEditCardNameButton: boolean = true;
  checklistData: any[] = [];
  checkItemsData: any[] = [];
  showChecklistHeading: boolean = true;
  showChecklistEditInputDiv: boolean = false;
  checklistName: string = "";
  showChecklistInputDiv: boolean = false;
  showCheckItemInputDiv: boolean = false;
  checkItemName: string = "";
  targetId: string = "";
  targetCheckItemId: string ="";
  showCheckItemName: boolean = true;
  showCheckItemEditInput: boolean = false;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private checklistService: ChecklistService) { 
    this.cardName = data.name;
    this.checklistService.getChecklists(data.id).subscribe(data => {
      console.log(data);
      this.checklistData = data;
    }); 
  }

  ngOnInit(): void {
  }

  toggleNameInputDiv() {
    this.showCardDialogHeading = !this.showCardDialogHeading;
    this.showCardDialogNameInputDiv = !this.showCardDialogNameInputDiv;
    this.showEditCardNameButton = !this.showEditCardNameButton;
  }

  handleInputChange(event) {
    this.cardName = event.target.value;
  }

  editCardName() {
    this.showCardDialogHeading = !this.showCardDialogHeading;
    this.showCardDialogNameInputDiv = !this.showCardDialogNameInputDiv;
    this.showEditCardNameButton = !this.showEditCardNameButton;
  }

  toggleChecklistEditInputDiv(event) {
    this.showChecklistHeading = !this.showChecklistHeading;
    this.showChecklistEditInputDiv = !this.showChecklistEditInputDiv;
    this.targetId = event.target.id;
  }

  handleChecklistInputChange(event) {
    this.checklistName = event.target.value;
  }

  editChecklistName() {
    this.showChecklistHeading = !this.showChecklistHeading;
    this.showChecklistEditInputDiv = !this.showChecklistEditInputDiv;
    this.checklistService.updateChecklist(this.targetId, this.checklistName).subscribe(() => {
      this.checklistData = this.checklistData.map(checklist => {
        if (checklist.id === this.targetId)
          {
            checklist.name = this.checklistName;
          }
        return checklist;
      });
    });
  }

  deleteThisChecklist(event) {
    this.checklistService.deleteChecklist(event.target.id).subscribe(() => {
      this.checklistData = this.checklistData.filter(checklist => checklist.id !== event.target.id);
    });
  }

  toggleChecklistInputDiv() {
    this.showChecklistInputDiv = !this.showChecklistInputDiv;
  }

  addNewChecklist() {
    this.showChecklistInputDiv = !this.showChecklistInputDiv;
    this.checklistService.addChecklist(this.data.id, this.checklistName).subscribe(data => {
      this.checklistData.push(data);
    })
  }

  toggleCheckItemInputDiv(event) {
    this.targetId = event.target.id;
    this.showCheckItemInputDiv = !this.showCheckItemInputDiv;
  }

  handleCheckItemInputChange(event) {
    this.checkItemName = event.target.value;
  }

  addNewCheckItem() {
    this.showCheckItemInputDiv = !this.showCheckItemInputDiv;
    this.checklistService.addCheckItem(this.targetId, this.checkItemName).subscribe(data => {
      this.checklistData = this.checklistData.map(checklist => {
        if (checklist.id === this.targetId)
          {
            checklist.checkItems.push(data);
          }
        return checklist;
      });
    });
  }

  toggleCheckItemEditInput(event) {
    this.showCheckItemName = !this.showCheckItemName;
    this.showCheckItemEditInput = !this.showCheckItemEditInput;
    this.targetCheckItemId = event.target.id;
  }

  handleCheckItemEditInputChange(event) {
    this.checkItemName = event.target.value;
    this.targetId = event.target.id;
  }

  editCheckItemName() {
    this.showCheckItemName = !this.showCheckItemName;
    this.showCheckItemEditInput = !this.showCheckItemEditInput;
    this.checklistService.updateCheckItemName(this.data.id, this.targetCheckItemId, this.checkItemName).subscribe(() => {
      this.checklistData = this.checklistData.map(checklist => {
        if (checklist.id === this.targetId)
          {
            checklist.checkItems.map(item => {
              if (item.id === this.targetCheckItemId)
                {
                  item.name = this.checkItemName;
                }
              return item;
            });
          }
        return checklist;
      });
    });
  }

  deleteThisCheckItem() {
    this.showCheckItemName = !this.showCheckItemName;
    this.showCheckItemEditInput = !this.showCheckItemEditInput;
    this.checklistService.deleteCheckItem(this.data.id, this.targetCheckItemId).subscribe(() => {
      this.checklistData = this.checklistData.map(checklist => {
        checklist.checkItems = checklist.checkItems.filter(checkItem => checkItem.id !== this.targetCheckItemId);
        return checklist;
      });
    });
  }
}
